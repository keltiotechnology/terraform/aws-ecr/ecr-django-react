output "load_balancer_arn" {
  value = module.vpc.aws_lb_arn
}

output "aws_lb_dns_name" {
  value = module.vpc.aws_lb_dns_name
}
