/*
 * Variable for VPC and Autoscaling group
*/
variable "region" {
  type        = string
  description = "AWS region"
}

variable "aws_profile" {
  type    = string
  description = "AWS credential profile"
  default = "default"
}

variable "namespace" {
  type = string
  description = "Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp'"
}

variable "vpc_name" {
  type = string
  description = "Solution name, e.g. 'app' or 'jenkins'"
}

variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR block"
}

variable "availability_zones" {
  type        = list(string)
  description = "List of availability zones"
}

variable "application_balancer_name" {
  type = string
  description = "Name of the application balancer"
}

variable "image_id" {
  type = string
  description = "AMI Image ID e.g. ami-0b440d17bfb7989dc. Please refer to this docs https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html"
}

variable "instance_type" {
  type = string
  description = "Instance type e.g. t2.micro"  
}

variable "autoscaling_group_name" {
  type = string
  description = "Name of the Autoscaling group for ECS"
}

variable "capacity_provider_name" {
  type = string
  description = "Name of the Capacity Provider for ECS cluster"
}

variable "min_size" {
  type = number
  description = "The minimum size of the Auto Scaling Group"
}

variable "max_size" {
  type = number
  description = "The maximum size of the Auto Scaling Group"
}

variable "desired_capacity" {
  type = number
  description = "The number of Amazon EC2 instances that should be running in the group"
}

variable "minimum_scaling_step_size" {
  type = number
  description = "The minimum size of the Auto Scaling Group"
}

variable "maximum_scaling_step_size" {
  type = number
  description = "The minimum size of the Auto Scaling Group"
}

variable "target_capacity" {
  type = number
  description = "Target utilization for the capacity provider. A number between 1 and 100."
}

/*
 * Variable for module ECR & ECS
*/
variable "ecs_cluster_name" {
    type = string
    description = "Name of ECS cluster"
}

variable "stage" {
  type        = string
  description = "stage"
}

variable "name" {
  type        = string
  description = "stage"
}

variable "image_names" {
  type        = list(any)
  description = "a list of image names will be deployed to ECR"
}

variable "docker_files" {
  type        = list(any)
  description = "a list of paths to dockerfile"
}

/*
 * Variables for module Task definition 
*/

variable "backend_app" {
  type = object({
    name = string,
    container_name = string,

    host_port = string,
    container_port = string,    
    
    container_cpu = number
    container_memory = number
    reserved_cpu = number
    reserved_memory = number

    container_command = list(string)

    desired_count = number
  })
}

variable "frontend_app" {
  type = object({
    name = string,
    container_name = string,
    
    host_port = string,
    container_port = string,
    
    container_command = list(string)

    container_cpu = number
    container_memory = number

    reserved_cpu = number
    reserved_memory = number

    desired_count = number
  })
}


variable "service_names" {
  type = list(string)
  description = "A list of services name. Ensure use only alphanumeric characters and hyphens."
}

variable "backend_heathcheck_path" {
    type = string
    description = "Heathcheck path for backend service"
}

variable "frontend_heathcheck_path" {
    type = string
    description = "Heathcheck path for backend service"
}

/*
 * Variables for DNS setup
*/
variable "hosted_zone_id" {
  type = string
  description = "ID of hosted zone in AWS Route53"
}

variable "domain_name" {
  type = string
  description = "The domain name alias to the Application Load balancer"
}