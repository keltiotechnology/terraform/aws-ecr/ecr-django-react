region = "eu-central-1"

namespace = "eg"
stage     = "test"
name      = "ecr"

# * Ensure placing the the sample order for docker file paths and image names
docker_files = ["/home/phatvo/WorkAtTekos/ecr-proj/django-ecs", 
                "/home/phatvo/WorkAtTekos/ecr-proj/react-sample-app"]

image_names  = ["hello-world-django-app", 
                "react-app"]

