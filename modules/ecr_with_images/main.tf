module "ecr" {
  source      = "cloudposse/ecr/aws"
  version     = "0.32.2"
  namespace   = var.namespace
  stage       = var.stage
  name        = var.name
  image_names = var.image_names
}

resource "null_resource" "docker_backend" {
  provisioner "local-exec" {
    command     = "chmod +x ${path.module}/ecr_push_image_template.sh && ${path.module}/ecr_push_image_template.sh"
    interpreter = ["/bin/bash", "-c"]

    environment = {
      REGION      = var.region
      HOST        = split("/", module.ecr.repository_url_map[var.image_names[0]])[0]
      IMAGE_NAME  = var.image_names[0]
      IMAGE_TAG   = "latest",
      DOCKER_FILE = var.docker_files[0]
    }
  }

  # TODO: Make this dynamic
  # * Uncomment to force building and pushing latest images to ECR
  # triggers = {
  #   always_run = "${timestamp()}"
  # }
}

resource "null_resource" "docker_frontend" {
  provisioner "local-exec" {
    command     = "chmod +x ${path.module}/ecr_push_image_template.sh && ${path.module}/ecr_push_image_template.sh"
    interpreter = ["/bin/bash", "-c"]

    environment = {
      REGION      = var.region
      HOST        = split("/", module.ecr.repository_url_map[var.image_names[1]])[0]
      IMAGE_NAME  = var.image_names[1]
      IMAGE_TAG   = "latest",
      DOCKER_FILE = var.docker_files[1]
    }
  }


  # TODO: Make this dynamic
  # * Uncomment to force building and pushing latest images to ECR
  # triggers = {
  #   always_run = "${timestamp()}"
  # }  
}

