# Description

This module allows us to create AWS Auto scaling with a launch template of ECS optimized instance.

Sample of variable input are in the "fixtures.tfvars" file. You may need to adjust it.


# Usage:
- terraform init
- terraform apply -var-file=fixtures.tfvars --auto-approve
- terraform destroy -var-file=fixtures.tfvars --auto-approve