vpc_id = "" # ID of your VPC 
public_subnet_ids = "" # List of public subnet Ids in VPC
autoscaling_group_name = "ecs-auto-scaling-group"
ecs_cluster_name = "my_cluster"
aws_security_group_ecs_id = "" # Id of your Security group Id for ECS