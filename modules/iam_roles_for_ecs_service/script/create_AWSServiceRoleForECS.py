#!/usr/bin/python
import sys
import boto3
import time
from botocore.config import Config


if __name__ == "__main__":
    client = boto3.client('ecs')
    cluster_name = 'empty_cluster_for_ecs_service_role'
    response = client.create_cluster(
        clusterName=cluster_name
    )

    if response['cluster']['clusterArn'] != '':
        print("Create empty cluster successfully")
    else:
        raise(Exception("Cannot create empty cluster successfully"))
    
    response = client.delete_cluster(
        cluster=cluster_name
    )

    if response['cluster']['status'] != 'ACTIVE':
        print("Delete empty cluster successfully")
    else:
        raise Exception("Cannot delete the empty cluster ", cluster_name)

    client = boto3.client('iam')
    response = client.get_role(RoleName='AWSServiceRoleForECS')

    if response['Role']['Path'] != '':
        print("Service role AWSServiceRoleForECS is created successfully")
        print('Role AWSServiceRoleForECS path is ', response['Role']['Path'])
    else:
        raise Exception("Service role AWSServiceRoleForECS is not created successfully")
