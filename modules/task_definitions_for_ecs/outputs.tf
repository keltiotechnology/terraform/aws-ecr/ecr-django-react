output "task_definition_arn" {
  description = "Backend definition ARN"
  value       = aws_ecs_task_definition.task.arn
}

output "container_json" {
  description = "Container definition in JSON format"
  value       = module.container.json_map_encoded_list
}
