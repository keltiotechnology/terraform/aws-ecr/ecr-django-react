/*
 * Learn about how ECS manages cpu and memory:
 *  https://aws.amazon.com/blogs/containers/how-amazon-ecs-manages-cpu-and-memory-resources/
*/
variable "application" {
  type = object({
    name = string,
    container_name = string,

    host_port = string,
    container_port = string,    
    
    container_cpu = number
    container_memory = number
    reserved_cpu = number
    reserved_memory = number

    container_command = list(string)
  })
}

variable "taskexecutionrole" {
  type = string
  description = "Name of the task execution role to associate with the task_definition"
}

variable "image_url" {
  type = map(string)
  description = "A map of application name to the image url. This information is provided by module ECR."
}

variable "region" {
  type        = string
  description = "AWS region"
}