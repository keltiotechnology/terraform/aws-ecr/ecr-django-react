variable "aws_profile" {
  type    = string
  description = "AWS credential profile"
  default = "default"
}

variable "namespace" {
  type = string
  description = "Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp'"
}

variable "vpc_name" {
  type = string
  description = "Solution name, e.g. 'app' or 'jenkins'"
}

variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR block"
}

variable "availability_zones" {
  type        = list(string)
  description = "List of availability zones"
}

variable "application_balancer_name" {
  type = string
  description = "Name of the application balancer"
}

variable "aws_security_load_balancer_id" {
  type = string
  description = "ID of security group for ecs"
}