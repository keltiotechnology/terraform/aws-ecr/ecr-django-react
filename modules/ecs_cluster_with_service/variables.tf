variable "vpc_id" {
 type = string
 description = "ID of VPC"
}

variable "cluster_name" {
  type = string
  description = "Cluster name"
}


variable "backend_service_name" {
  type = string
  description = "Name of the backend service"
}

variable "frontend_service_name" {
  type = string
  description = "Name of the frontend service"
}

variable "backend_task_definition_arn" {
    type = string
    description = "ARN of task definition for backend service"
}

variable "frontend_task_definition_arn" {
    type = string
    description = "ARN of task definition for frontend service"
}

variable "backend_container_name" {
  type = string
  description = "backend container name"
}

variable "backend_container_port" {
  type = string
  description = "backend container port"
}

variable "frontend_container_name" {
  type = string
  description = "frontend container name"
}

variable "frontend_container_port" {
  type = string
  description = "frontend container port"
}

variable "backend_desired_count" {
    type = number
    description = "Number of instances of the task definition of backend"
}

variable "frontend_desired_count" {
    type = number
    description = "Number of instances of the task definition of frontend"
}

variable "backend_heathcheck_path" {
    type = string
    description = "Heathcheck path for backend service"
}

variable "frontend_heathcheck_path" {
    type = string
    description = "Heathcheck path for backend service"
}

variable "acm_certificate_arn" {
    type = string
    description = "ARN to SSL certificate in AWS ACM"    
}

variable "load_balancer_arn" {
    type = string
    description = "ARN to Load balancer"
}




# variable "aws_lb_dns_name" {
#   type = string
#   description = "DNS name of AWS Load balancer"
# }

# variable "aws_lb_zone_id" {
#   type = string
#   description = "Hosted zone Id of AWS Load balancer"
# }




/*
module.task_definition.django_task_definition_arn
module.task_definition.backend_container_name
module.task_definition.backend_container_port # 80
aws_acm_certificate_validation.ecs.certificate_arn
*/
