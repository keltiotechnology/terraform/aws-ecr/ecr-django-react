
resource "aws_iam_role" "ecs-service-role" {
  name               = "ecs_service_role_test"
  assume_role_policy = file("${path.module}/policies/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs-service-role-policy" {
  name   = "ecs_service_role_policy"
  policy = file("${path.module}/policies/ecs-service-role-policy.json")
  role   = aws_iam_role.ecs-service-role.id
}


resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = aws_iam_role.ecs-service-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

